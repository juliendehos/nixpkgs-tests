#include <iostream>

int f(int x) {
    int y;
    y = x*2;
    return y;
}

int main() {
    int a;
    a = 21;
    int b;
    b = f(a);
    std::cout << b << std::endl;
    return 0;
}

