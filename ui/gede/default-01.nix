
let

  # rev = "fbc01ec1bb370a2922948fc50cc215b9f74c16e8";
  # pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz") {};
  pkgs = import <nixpkgs> {};

  gede = pkgs.libsForQt5.callPackage ./gede.nix {
    qmake = pkgs.qt5.qmake;
    lib = pkgs.lib;
  };

in 
  gede

  # pkgs.mkShell {
  #   name = "mygede";
  #   buildInputs = [
  #     gede
  #     # pkgs.qt5Full
  #   ];
  # }

