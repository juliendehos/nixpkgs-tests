let

  # rev = "4144e371dd366ba2c20458f03166df80ee335645";
  # pkgs = import (fetchTarball {
  #   url = "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";
  #   sha256 = "0mazz4rchqrbaacqvvwlfimnjm86z8v3qqk8fckixp9ls05445nk";
  # }) {};

  pkgs = import <nixpkgs> {};

  gede = pkgs.libsForQt5.callPackage ./gede.nix {
    qmake = pkgs.qt5.qmake;
    lib = pkgs.lib;
  };

in
  pkgs.mkShell {
    buildInputs = [
      gede
      # pkgs.ctags
      # pkgs.qt5Full
      # pkgs.xorg.libxcb
    ];
  }



