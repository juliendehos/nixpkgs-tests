let
  # rev = "1cd122e7b6e404dbda80704774ad1d180f02cde6";
  # sha256 = "0fix72ybl1p75nn9jx35i0wgrq4k6prhhl4z4x7zn9bmashbqxvf";
  rev = "69b41f98809c7a3bfe79dae28c31917927ee9f64";
  sha256 = "1c4brp8yyj257a8a8ximjl45fa9fkkgasnbdfnkss79iymxy4ma8";
  pkgs = import (fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";
    inherit sha256;
  }) {};
in with pkgs; mkShell {
    buildInputs = [
      pfstools
      gnumake
    ];
  }

