{ lib
, buildPythonPackage
, fetchFromGitHub
, numpy
}:

buildPythonPackage rec {
  pname = "cma";
  version = "2.7.0";

  src = fetchFromGitHub {
    owner = "CMA-ES";
    repo = "pycma";
    rev = "r${version}";
    sha256 = "0c26969pcqj047axksfffd9pj77n16k4r9h6pyid9q3ah5zk0xg3";
  };

  propagatedBuildInputs = [
    numpy
  ];

  doCheck = false;

}

