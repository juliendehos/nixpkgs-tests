let
  config = {
    allowUnfree = true;
    cudaSupport = true;
    enableParallelBuilding = true;
  };
  pkgs = import <nixpkgs> { inherit config; };

  pypkgs = pkgs.python38.pkgs;

in pkgs.mkShell {

  buildInputs = with pypkgs; [

    pkgs.linuxPackages.nvidia_x11

    pypkgs.cma
    pypkgs.gym 
    pypkgs.matplotlib
    pypkgs.numpy
    pypkgs.pandas
    pypkgs.pytorchWithCuda
    pypkgs.torchvision
    pypkgs.typing-extensions

    (pkgs.callPackages ./bayesian-optimization.nix { inherit buildPythonPackage scikitlearn scipy; })
    (pkgs.callPackages ./fcmaes.nix { inherit buildPythonPackage fetchPypi scipy; })
    (pkgs.callPackages ./tensorboardX.nix { inherit buildPythonPackage numpy protobuf six; })

  ];

}

