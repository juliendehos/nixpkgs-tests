{ lib
, buildPythonPackage
, fetchFromGitHub
, scikitlearn
, scipy
}:

buildPythonPackage rec {
  pname = "bayesian-optimization";
  version = "1.1.0";

  src = fetchFromGitHub {
    owner = "fmfn";
    repo = "BayesianOptimization";
    rev = "v${version}";
    sha256 = "0ylip9xdi0cjzmdayxxpazdfaa9dl0sdcl2qsfn3p0cipj59bdvd";
  };

  propagatedBuildInputs = [
    scikitlearn
    scipy
  ];

  doCheck = false;
}
