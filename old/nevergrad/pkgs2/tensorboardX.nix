{ lib
, buildPythonPackage
, numpy
, protobuf
, six
}:

buildPythonPackage rec {
  pname = "tensorboardX";
  version = "1.8";
  src = fetchTarball "https://github.com/lanpa/tensorboardX/archive/v1.8.tar.gz";
  propagatedBuildInputs = [
    six
    protobuf
    numpy
  ];
  doCheck = false;
}

