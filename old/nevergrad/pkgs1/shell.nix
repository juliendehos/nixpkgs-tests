let
  pkgs = import <nixpkgs> {};

  pypkgs = pkgs.python38.pkgs;

in pkgs.mkShell {

  buildInputs = with pypkgs; [

    pypkgs.cma
    pypkgs.gym 
    pypkgs.matplotlib
    pypkgs.numpy
    pypkgs.pandas
    pypkgs.typing-extensions

    (pkgs.callPackages ./n/bayesian-optimization.nix {
      inherit buildPythonPackage scikitlearn scipy; })

  ];

}


