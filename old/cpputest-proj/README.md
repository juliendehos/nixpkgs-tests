
```
$ nix-shell
this path will be fetched (0.18 MiB download, 1.53 MiB unpacked):
  /nix/store/ak4fz5vzzjcpddrm0hrzp5z8xw4qq570-cpputest-4.0
copying path '/nix/store/ak4fz5vzzjcpddrm0hrzp5z8xw4qq570-cpputest-4.0' from 'https://cache.nixos.org'...


[nix-shell]$ cmake -B build .
-- The C compiler identification is GNU 12.2.0
-- The CXX compiler identification is GNU 12.2.0
-- Detecting C compiler ABI info
...


[nix-shell]$ cmake --build build
[ 50%] Building CXX object CMakeFiles/cpputest-proj.dir/main.cpp.o
[100%] Linking CXX executable cpputest-proj
[100%] Built target cpputest-proj


[nix-shell]$ ./build/cpputest-proj 
.
OK (1 tests, 1 ran, 1 checks, 0 ignored, 0 filtered out, 0 ms)
```
