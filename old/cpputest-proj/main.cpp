#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(FirstTestGroup) {};

TEST(FirstTestGroup, FirstTest) {
   CHECK_EQUAL(42, 42);
}

int main(int argc, char ** argv) {
   return CommandLineTestRunner::RunAllTests(argc, argv);
}

