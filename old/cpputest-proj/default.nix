
with import <nixpkgs> {};
stdenv.mkDerivation {
    name = "cpputest-proj";
    src = ./.;
    buildInputs = [
        cmake
        cpputest
    ];
}

