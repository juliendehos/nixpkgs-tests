let
  pkgs = import <nixpkgs> {};
  gtksourceviewmm = pkgs.callPackage ./gtksourceviewmm.nix {};
in
  pkgs.mkShell {
    buildInputs = [
      pkgs.pkgconfig
      pkgs.gnome3.gtkmm
      pkgs.gnome3.gtksourceview4
      gtksourceviewmm
    ];
  }


