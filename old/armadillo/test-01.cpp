#include <iostream>
#include <armadillo>

using namespace std;
using namespace arma;

int main() {

    mat A = randu<mat>(4,5);
    mat B = randu<mat>(4,5);

    cout << A*B.t() << endl;

    arma_version ver;
    cout << "ARMA version: "<< ver.as_string() << endl;

    return 0;
}

