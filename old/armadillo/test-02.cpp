#include <iostream>

#define ARMA_USE_HDF5
// #define ARMA_DONT_USE_WRAPPER
//#include <hdf5.h>
#include <armadillo>

using namespace std;
using namespace arma;

int main() {
    mat A = randu<mat>(4,5);
    A.save("A.csv", csv_ascii);
    A.save("A.h5", hdf5_binary);
    cout << A << endl;
    return 0;
}

