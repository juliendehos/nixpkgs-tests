let
  pkgs = import <nixpkgs> {};
  armadillo = pkgs.callPackage ./armadillo.nix {};
in
  pkgs.mkShell {
    buildInputs = [
      armadillo
      pkgs.hdf5
    ];
  }

