
let

  pkgs = import <nixpkgs> {};

  plantuml = pkgs.callPackage ./plantuml.nix {};

in

  plantuml

