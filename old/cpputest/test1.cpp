#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(FirstTestGroup) {};

TEST(FirstTestGroup, FirstTest) {
   CHECK_EQUAL(42, 42);
}

int main(int ac, char ** av) {
   return CommandLineTestRunner::RunAllTests(ac, av);
}

