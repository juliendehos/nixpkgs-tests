let
  rev = "5739fd1";
  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz") {};
in with pkgs; mkShell {
  buildInputs = [
    cpputest
    gnumake
  ];
}

