let
  rev = "fa845b3cd72143bc4b249fcf44aa2cb9c0964076";
  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz") {};
in (pkgs.python3.withPackages ( ps: with ps; [ bayesian-optimization pip ])).env

