let
  #rev = "7a708a3f7cc5bbd9809db4ff92581470feeaddea";
  rev = "327335ce8f7d78cf01fab235c324f2abd9f46386";
  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz") {};
in (pkgs.python3.withPackages(ps: [ ps.gym ])).env

