with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "DGEdit";

  src = fetchGit {
    url = "http://git.drumgizmo.org/dgedit.git";
  };

  buildInputs = [
    autoconf
    automake
    gettext
    gnumake
    libao
    libsndfile
    libtool
    m4
    pkgconfig
    qt5Full
    qt5.wrapQtAppsHook
  ];

  configurePhase = ''
     ./autogen.sh
     ./configure --prefix $out
  '';

  enableParallelBuilding = true;

}

