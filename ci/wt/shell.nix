let
  rev = "0534d79b7e56c1d3f189faae728cbafe2a8d7e17";
  sha256 = "sha256:1dv6ag3r66zsp5wgv4hfr2sd3jv633592hbv1x1k3ymc5485vnh1";
  pkgs = import (fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";
    inherit sha256;
  }) {};
in with pkgs; mkShell {
    buildInputs = [
      gnumake
      wt
      xorg.libICE
      xorg.libSM
    ];
    shellHooks = ''
      export WT_ROOT=${wt}
      echo $WT_ROOT
    '';
  }

