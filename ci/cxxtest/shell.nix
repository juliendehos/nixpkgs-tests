let
  rev = "4c116b77c7bbc36d4c7ec66c7aa4c4007306c2f3";
  sha256 = "1c8lad6nsid01f5qjwhdw8i2x698z1z1q2c6kjnyr5a34cbjnghy";
  pkgs = import (fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";
    inherit sha256;
  }) {};
in with pkgs; mkShell {
    buildInputs = [
      cmake
      cxxtest
      gnumake
    ];
  }

