#!/bin/sh

cd $(dirname "$0")
nix-shell --pure --run "make -B -C test-make" || exit 1

nix-shell --pure --run "cmake -S test-cmake build" || exit 1
nix-shell --pure --run "cmake --build build" || exit 1
nix-shell --pure --run "./build/unittests.out" || exit 1

