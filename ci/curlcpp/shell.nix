let
  rev = "119a3ab";
  pkgs = import (fetchTarball {
    url = "http://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";
    sha256 = "11j7b89pzzi9l56gniw12x58309xq420xhrgq0mpca89yljfc8v2";
  }) {};
in with pkgs; mkShell {
  buildInputs = [
    curl
    curlcpp
    gcc
    gnumake
  ];
}

