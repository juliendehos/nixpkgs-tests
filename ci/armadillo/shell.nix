let
  rev = "9cf81429c3bd727db76ee3d032e349ccbf0d0d3f";
  sha256 = "sha256:0hh4f8y2yp3dykjvzab38jv88jzp5jaaawgnpydrqc5akw2is31p";
  pkgs = import (fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";
    inherit sha256;
  }) {};
in with pkgs; mkShell {
    buildInputs = [
      armadillo
      hdf5
      gnumake
    ];
  }

