#include <iostream>

#define ARMA_USE_HDF5
#include <armadillo>

using namespace std;
using namespace arma;

int main() {

    mat A_csv;
    cout << "\nA.csv" << endl;
    A_csv.load("A.csv");
    cout << A_csv << endl;

    mat A_h5;
    cout << "\nA.h5" << endl;
    A_h5.load("A.h5", hdf5_binary);
    cout << A_h5 << endl;

    return 0;
}

