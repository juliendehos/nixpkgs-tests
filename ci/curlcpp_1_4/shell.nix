let
  rev = "7ce6fee";
  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz") {};

in with pkgs; mkShell {
  buildInputs = [
    curl
    curlcpp
    gcc
    gnumake
  ];
}

