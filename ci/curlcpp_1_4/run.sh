#!/bin/sh

cd $(dirname "$0")
nix-shell --pure --run "g++ -Wall -Wextra -o test1.out test1.cpp -lcurlcpp -lcurl" || exit 1
./test1.out || exit 1

