let
  rev = "b3151eff6372e1e8ce52e07f69338d5952b6174c";
  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz") {};
  #pkgs = import <nixpkgs>{};
in (pkgs.python3.withPackages ( ps: with ps; [ bayesian-optimization pip ])).env

