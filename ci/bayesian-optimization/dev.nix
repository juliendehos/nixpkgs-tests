with import <nixpkgs> { config = { allowBroken = true; }; };

(let

  python = let

    packageOverrides = self: super: {

      bayesian-optimization = self.buildPythonPackage rec {
        pname = "bayesian-optimization";
        version = "1.2.0";
        src = self.fetchPypi {
          inherit pname version;
          sha256 = "0df95dqq8mwdb8cfh96rr9av9fgz02lv55aj2hffw96cnvs3mzf2";
        };
        propagatedBuildInputs = [
          self.scikitlearn
          self.scipy
        ];
      };

    };

  in pkgs.python3.override { inherit packageOverrides; self = python; };

in python.withPackages(ps: [ ps.bayesian-optimization ])).env

